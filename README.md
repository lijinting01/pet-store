# pet-store

#### 项目介绍

Pet-store for introducing testing.

- Unit testing using JUnit, Mockito, Maven Surefire.
- Integration Testing Using **Spring Boot** and **Rest-Assured**
- Check the coverage using **jacoco-maven-plugin**

#### 安装教程

> Maven installation is **OPTIONAL**.
>
> Internet connection is **MANDATORY**.
>
> Lombok configuration is **MANDATORY** if you are using IDE like Intellij IDEA or Eclipse

- Trying unit testing run : 
```
mvnw clean test
```
- Trying integration testing run :
```
mvnw clean verify
```

- Running the application simply run : 
```
mvnw spring-boot:run
```
and then visit [http://localhost:8080](http://localhost:8080)

- If you are working behind a proxy server, run the command below before building the application.
 
```
set MAVEN_OPTS=-Dhttp.proxyHost=proxyhost -Dhttp.proxyPort=8080 -Dhttps.proxyHost=proxyhost -Dhttps.proxyPort=8080
```

#### 使用说明



#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

