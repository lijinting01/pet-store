package io.demo.petstore.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.util.StringUtils;

@Slf4j
public class RandomStringBuilderTest {

  @Rule
  public final ExpectedException expectedException = ExpectedException.none();

  @Test
  public void build() {
    String s = RandomStringBuilder.newBuilder()
        .useDigit()
        .useLowerCase()
        .useUpperCase()
        .build(15);
    log.info("{}", s);
    assertThat(s).containsPattern("[A-Za-z0-9]{15}");
  }

  @Test
  public void build1() {
    String s = RandomStringBuilder.newBuilder()
        .useDigit()
        .useLowerCase()
        .useUpperCase()
        .build(15, 20);
    log.info("{}", s);
    assertThat(s).containsPattern("[A-Za-z0-9]{15,20}");
  }

  @Test
  public void build2() {
    String s = RandomStringBuilder.newBuilder()
        .useWhitespace()
        .build(25);
    log.info("{}", s);
    assertThat(StringUtils.trimAllWhitespace(s)).isEmpty();
  }


  @Test
  public void build3() {
    expectedException.expect(IllegalStateException.class);
    expectedException.expectMessage(is(equalTo(
        "No character set/range specified, invoke .useDigit()/.useUpperCase() etc. before invoking .build()")));
    RandomStringBuilder.newBuilder()
        .build(2);
  }

  @Test
  public void build4() {
    String s = RandomStringBuilder.newBuilder()
        .useSymbol()
        .build(22, 100);
    log.info("{}", s);
    Pattern pattern = Pattern.compile("[!@#$%^&*()\\[\\]{};:,.?_+=\\-~]{22,100}");
    assertThat(s).containsPattern(pattern);
  }

  @Test
  public void printAll() {
    String s = RandomStringBuilder.newBuilder()
        .useSymbol()
        .useDigit()
        .useLowerCase()
        .useUpperCase()
        .build(1000, 10000);
    log.info("{}", s);
    Pattern pattern = Pattern.compile("[A-Za-z0-9!@#$%^&*()\\[\\]{};:,.?_+=\\-~]{1000,10000}");
    assertThat(s).containsPattern(pattern);
  }
}