package io.demo.petstore.account;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.demo.petstore.util.RandomStringBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * <pre>
 *     范例
 *     <ol>
 *         <li>演示如何mock</li>
 *         <li>演示如何设计正例反例</li>
 *     </ol>
 * </pre>
 *
 * @author lijinting01
 */
@RunWith(MockitoJUnitRunner.class)
public class UserExistValidatorTest {

  @Mock
  private AccountDao accountDao;

  private String alreadyExists;

  private String notExists;

  private UserExistValidator userExistValidator;

  @Before
  public void setUp() {

    alreadyExists = RandomStringBuilder.newBuilder()
        .useDigit()
        .useLowerCase()
        .build(15);

    notExists = RandomStringBuilder.newBuilder()
        .useDigit()
        .useUpperCase()
        .build(20);

    doReturn(new Account())
        .when(accountDao).findByUsername(alreadyExists);

    doReturn(null)
        .when(accountDao).findByUsername(notExists);
    userExistValidator = new UserExistValidator(accountDao);
  }

  /**
   * 正例：测试普通情况
   */
  @Test
  public void validateSuccess() {

    Account account = mock(Account.class);
    when(account.getUsername()).thenReturn(notExists); // the username is right

    // the return value is true
    assertThat(userExistValidator.test(account)).isTrue();

    // verify that accountDao was called exactly once
    // the code below is equivalent to verify(accountDao).findByUsername(anyString());
    verify(accountDao, times(1)).findByUsername(notExists);
  }

  /**
   * 反例
   */
  @Test
  public void validFail() {
    Account account = mock(Account.class);
    when(account.getUsername()).thenReturn(alreadyExists); // the username is right

    // the return value is false
    assertThat(userExistValidator.test(account)).isFalse();

    // verify that accountDao was called exactly once
    verify(accountDao, times(1)).findByUsername(alreadyExists);
  }
}