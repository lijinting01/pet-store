package io.demo.petstore.account;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import io.demo.petstore.customer.Customer;
import io.demo.petstore.customer.CustomerDao;
import io.demo.petstore.util.RandomStringBuilder;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.springframework.data.domain.Example;

/**
 * <pre>
 * TODO: 作业编写单元测试验证 {@link AccountServiceImpl#register(Account)}
 * <ol>
 *     <li>
 *         单元 {@link AccountServiceImpl#register(Account)} 覆盖率达到100%
 *     </li>
 *     <li>
 *         {@link AccountServiceImpl#register(Account)} 测试用例100%通过
 *     </li>
 *     <li>
 *         对底层依赖 {@link #accountDao} 进行 {@link org.mockito.Mockito#mock(Class)}
 *     </li>
 *     <li>
 *         对底层依赖 {@link AccountDao#findByUsername(String)} 调用次数进行 {@link org.mockito.Mockito#verify(Object)}
 *     </li>
 *     <li>
 *         <strong>提示：</strong>
 *         注意Validator的顺序，根据顺序来模拟才能引发相应异常的Account
 *     </li>
 * </ol>
 * </pre>
 *
 * @author lijinting01
 */
@Slf4j
public class AccountServiceImplTest {

  private String randomValidUsername;

  private String randomValidPassword;

  private String randomAlreadyExistsUsername;

  private Account validAccount;

  private AccountDao accountDao;

  private AccountServiceImpl accountService;


  @Before
  public void setUp() {
    randomValidUsername = RandomStringBuilder.newBuilder()
        .useLowerCase()
        .useDigit()
        .build(8);

    randomAlreadyExistsUsername = RandomStringBuilder.newBuilder()
        .useLowerCase()
        .useDigit()
        .useUpperCase()
        .build(15);

    randomValidPassword = RandomStringBuilder.newBuilder().useDigit().build(8)
        + RandomStringBuilder.newBuilder().useLowerCase().build(8);

    RandomStringBuilder builder = RandomStringBuilder.newBuilder()
        .useLowerCase()
        .useUpperCase();
    Customer customer = Customer.builder()
        .lastName(builder.build(20))
        .identity(builder.build(18))
        .build();
    validAccount = Account.builder()
        .username(randomValidUsername)
        .password(randomValidPassword)
        .customer(customer)
        .build();

    accountDao = mock(AccountDao.class);
    doReturn(null)
        .when(accountDao).findByUsername(randomValidUsername);

    doReturn(validAccount)
        .when(accountDao).findByUsername(randomAlreadyExistsUsername);

    doReturn(validAccount)
        .when(accountDao).save(any(Account.class));

    CustomerDao customerDao = mock(CustomerDao.class);

    // no matter what you pass~~
    doReturn(null)
        .when(customerDao).findOne(Matchers.<Example<Customer>>any());

    accountService = new AccountServiceImpl(accountDao, customerDao);
    accountService.afterPropertiesSet();
  }

  /**
   * 正例
   */
  @Test
  public void registerSuccess() {
    Account account = accountService.register(validAccount);

    verify(accountDao).findByUsername(randomValidUsername);
    verify(accountDao).save(validAccount);

    assertThat(account.getUsername()).isEqualTo(randomValidUsername);
    assertThat(account.getPassword()).isEqualTo(randomValidPassword);
  }

  /**
   * <pre>
   * TODO: 作业
   * 反例：验证用户名重复的情况下注册
   * <ol>
   *     <li>失败并抛出 {@link AccountAlreadyExistsException}</li>
   * </ol>
   * </pre>
   */
  @Test(expected = AccountAlreadyExistsException.class)
  public void registerFailWithAlreadyExistUsername() {
    assertThat(randomAlreadyExistsUsername).isNotEmpty();
    throw new AccountAlreadyExistsException();
  }

  /**
   * <pre>
   * TODO: 作业
   * 反例：验证用户名为空时的情况下注册
   * <ol>
   *     <li>失败并抛出 {@link InvalidUsernameException}</li>
   * </ol>
   * </pre>
   */
  @Test(expected = InvalidUsernameException.class)
  public void registerFailWithBlankUsername() {
    throw new InvalidUsernameException();
  }

  /**
   * TODO: 作业
   *
   * 反例：验证没有填写客户信息情况下的注册
   * <ol>
   * <li>失败并抛出{@link InvalidCustomerException}</li>
   * </ol>
   */
  @Test(expected = InvalidCustomerException.class)
  public void registerFailWithNoCustomer() {
    throw new InvalidCustomerException();
  }
}