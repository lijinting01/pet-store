package io.demo.petstore;

import static org.assertj.core.api.Assertions.assertThat;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @author lijinting01
 */
@Slf4j
public class PetStoreApplicationIT {

  @Test
  public void checkIndexTitle() throws IOException {
    try (final WebClient webClient = new WebClient()) {
      final HtmlPage indexPage = webClient.getPage("http://localhost:8080/");
      assertThat(indexPage.getTitleText()).isEqualTo("What can i help u?!");
      assertThat(indexPage.getCharset()).isEqualTo(StandardCharsets.UTF_8);
      assertThat(indexPage.getElementById("container")).isNotNull();
      log.info("All Containers : {}", indexPage.getElementsById("container"));
    }
  }
}
