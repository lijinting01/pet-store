package io.demo.petstore.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.demo.petstore.account.Account;
import io.demo.petstore.account.AccountService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * @author lijinting01
 */
@RunWith(MockitoJUnitRunner.class)
public class RegisterControllerTest {

  private RegisterController registerController;

  @Mock
  private AccountService accountService;

  private MockMvc mockMvc;

  private ObjectMapper objectMapper;

  @Before
  public void setUp() {
    objectMapper = new ObjectMapper();

    registerController = new RegisterController(accountService);
    mockMvc = MockMvcBuilders.standaloneSetup(registerController)
        .setControllerAdvice(new ClientErrorHandler(), new ServerErrorHandler())
        .build();
  }

  @Test
  public void registerSuccess() {
    RegisterRequest registerRequest = RequestBodies.single().randomValidRegisterRequest();
    when(accountService.register(registerRequest)).thenReturn(registerRequest);

    Account account = registerController.register(registerRequest);
    assertThat(account.getUsername()).isEqualTo(registerRequest.getUsername());
  }

  @Test
  public void registerSuccess1() throws Exception {
    RegisterRequest registerRequest = RequestBodies.single().randomValidRegisterRequest();
    when(accountService.register(registerRequest)).thenReturn(registerRequest);

    mockMvc.perform(
        post("/account/register")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(registerRequest)))
        .andDo(print())
        .andDo(log())
        .andExpect(status().isCreated());
  }
}