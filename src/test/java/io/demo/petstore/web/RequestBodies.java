package io.demo.petstore.web;

import io.demo.petstore.customer.Customer;
import io.demo.petstore.util.RandomStringBuilder;

/**
 * 随机创建RequestBody以满足测试需要
 */
class RequestBodies {

  private static RequestBodies single;

  private final RandomStringBuilder numericBuilder;

  private final RandomStringBuilder otherBuilder;


  static synchronized RequestBodies single() {
    if (single == null) {
      single = new RequestBodies();
    }
    return single;
  }

  private RequestBodies() {
    numericBuilder = RandomStringBuilder.newBuilder().useDigit();
    otherBuilder = RandomStringBuilder.newBuilder().useDigit().useLowerCase();
  }

  RegisterRequest randomValidRegisterRequest() {
    Customer customer = Customer.builder()
        .identity(numericBuilder.build(18))
        .lastName(otherBuilder.build(10))
        .firstName(otherBuilder.build(10))
        .build();

    String username = otherBuilder.build(15);
    String password = otherBuilder.build(22);

    RegisterRequest registerRequest = new RegisterRequest();
    registerRequest.setCustomer(customer);
    registerRequest.setPassword(password);
    registerRequest.setUsername(username);
    return registerRequest;
  }
}
