/*
 * 通用响应体的数据结构
 */

/**
 *
 */
function code() {
  if (this.res.code) {
    return this.res.code;
  }
  return -999;
}

/**
 *
 * @returns
 */
function objects() {
  if (this.res.objects) {
    if ($.isArray(this.res.objects)) {
      return this.res.objects;
    }
    return [this.res.objects];
  }
  return [];
}

/**
 *
 * @returns 获取第一个对象作为唯一结果
 */
function singleObject() {
  return this.objects().length === 0 ? null : this.objects()[0];
}

/**
 * code 为负数则表示有错误
 *
 * @returns
 */
function hasError() {
  return this.res.code < 0;
}

/**
 * @returns
 */
function message() {
  return this.res.message || "";
}

/**
 * res为0则表示一切顺利
 *
 * @returns
 */
function isOk() {
  return this.res.code === 0;
}

/**
 * 返回值大于0表示请求不正确
 *
 * @returns
 */
function isBadRequest() {
  return this.res.code > 0;
}

/**
 * 响应是否有数据
 *
 * @returns
 */
function hasObjects() {
  return this.res.objects && Array.isArray(this.res.objects)
      && this.res.objects.length > 0
}

/**
 * 获取响应数据的长度
 *
 * @returns
 */
function objectsLength() {
  return this.hasObjects() ? this.res.objects.length : 0;
}

/**
 *
 * @param res
 * @returns
 */
function ResponseObject(res) {
  this.res = res || {
    code: -998,
    message: "",
    objects: []
  };
  if (typeof (res) === "string") {
    this.res = JSON.parse(res);
  }

  this.code = code;
  this.message = message;
  this.objects = objects;
  this.hasError = hasError;
  this.isBadRequest = isBadRequest;
  this.isOk = isOk;
  this.hasObjects = hasObjects;
  this.objectsLength = objectsLength;
  this.singleObject = singleObject;
}