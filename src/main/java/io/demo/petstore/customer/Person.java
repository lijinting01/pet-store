package io.demo.petstore.customer;

public interface Person {

  Long getId();

  String getFirstName();

  String getLastName();

  GenderType getGenderType();

  String getIdentity();
}
