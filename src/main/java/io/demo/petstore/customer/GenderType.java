package io.demo.petstore.customer;

/**
 * @author lijinting01
 */
public enum GenderType {
  UNKNOWN,

  MALE,

  FEMALE
}
