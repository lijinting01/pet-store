package io.demo.petstore.customer;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;
import lombok.ToString;

/**
 * @author lijinting01
 */
@Builder
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "T_CUSTOMER",
    uniqueConstraints = {@UniqueConstraint(columnNames = "identity")})
public class Customer implements Person {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String firstName;

  private String lastName;

  private GenderType genderType;

  /**
   * Identity code of the customer
   */
  private String identity;

  @Singular
  @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  private List<Address> addresses;
}
