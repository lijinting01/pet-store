package io.demo.petstore.util;

import java.util.function.Predicate;

/**
 * @author lijinting01
 */
@FunctionalInterface
public interface Validator<T> extends Predicate<T> {

  /**
   * @param object the object to be tested
   * @return true if the object complies with the rule of validator. false, otherwise.
   */
  boolean test(T object);
}
