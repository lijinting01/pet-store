package io.demo.petstore.account;

import io.demo.petstore.util.Validator;

interface AccountValidator extends Validator<Account> {

  /**
   * The respective exception of current validator. Client code is able to get the exception when
   * {@link #test(Object)} returns false
   *
   * @return Any subtype of {@link AccountException}
   */
  AccountException accountException();
}
