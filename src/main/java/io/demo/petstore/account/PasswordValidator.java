package io.demo.petstore.account;

import java.util.Optional;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

/**
 * @author lijinting01
 */
public class PasswordValidator implements AccountValidator {

  /**
   * <ol>
   * <li>长度在{@literal 8-24}位</li>
   * <li>字符内容包括大小写字母;数字;{@literal !@#$%^&*()[]{}|}</li>
   * </ol>
   */
  private static final String PATTERN = "^([a-zA-Z0-9!@#$%^&*()\\[\\]{}|]{8,24})$";

  @Override
  public boolean test(Account account) {

    String password = Optional.ofNullable(account)
        .map(Account::getPassword)
        .filter(StringUtils::isNotBlank)
        .orElse(null);

    if (password == null) {
      return false;
    }

    return Pattern.matches(PATTERN, password);
  }

  @Override
  public AccountException accountException() {
    return new PasswordConstraintException();
  }
}
