package io.demo.petstore.account;

class PasswordConstraintException extends AccountException {

  private static final long serialVersionUID = 3220020239775990395L;
}
