package io.demo.petstore.account;

import io.demo.petstore.customer.Customer;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

public class CustomerValidator implements AccountValidator {

  @Override
  public boolean test(Account object) {

    Customer customer = Optional.ofNullable(object)
        .map(Account::getCustomer)
        .orElse(null);

    return customer != null
        && StringUtils.isNotBlank(customer.getLastName())
        && StringUtils.isNotBlank(customer.getIdentity());
  }

  @Override
  public AccountException accountException() {
    return new InvalidCustomerException();
  }
}
