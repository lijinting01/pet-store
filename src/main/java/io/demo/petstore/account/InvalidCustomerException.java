package io.demo.petstore.account;

class InvalidCustomerException extends AccountException {

  private static final long serialVersionUID = 9139525154558019035L;
}
