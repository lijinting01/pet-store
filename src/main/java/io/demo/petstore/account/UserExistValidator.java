package io.demo.petstore.account;

import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

/**
 * <ul>
 * <li>username should not be blank</li>
 * <li>username </li>
 * </ul>
 *
 * @author lijinting01
 */
public class UserExistValidator implements AccountValidator {


  private final AccountDao accountDao;

  public UserExistValidator(AccountDao accountDao) {
    this.accountDao = accountDao;
  }

  @Override
  public boolean test(Account account) {

    String username = Optional.ofNullable(account)
        .map(Account::getUsername)
        .map(String::trim)
        .orElse(StringUtils.EMPTY);

    return null == accountDao.findByUsername(username);
  }

  @Override
  public AccountException accountException() {
    return new AccountAlreadyExistsException();
  }
}
