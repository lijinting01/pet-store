package io.demo.petstore.account;

/**
 * Implies the account already exists
 *
 * @author lijinting01
 */
class AccountAlreadyExistsException extends AccountException {

  private static final long serialVersionUID = 8783981218685230676L;
}
