package io.demo.petstore.account;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author lijinting01
 */
public interface AccountDao extends JpaRepository<Account, Long> {

  /**
   * Find by username
   *
   * @param username the specified username
   * @return null if none is found, the unique result if single result is found.
   */
  Account findByUsername(String username);
}
