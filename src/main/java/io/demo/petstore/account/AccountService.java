package io.demo.petstore.account;

/**
 * @author lijinting01
 */
public interface AccountService {

  /**
   * Register a new account.
   *
   * @param account The account to be persisted
   * @return persisted account
   * @throws AccountAlreadyExistsException <pre>
   *     If the specified account has the same username with another existing account
   * </pre>
   * @throws InvalidUsernameException <pre>
   *     If the username is shorter than 6 after trim
   * </pre>
   * @throws InvalidCustomerException <pre>
   *     If the customer and it's attributes are not specified, e.g. customer's last name or identity number
   * </pre>
   */
  Account register(Account account);
}
