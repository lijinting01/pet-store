package io.demo.petstore.account;

class InvalidUsernameException extends AccountException {

  private static final long serialVersionUID = 4767628028391580593L;
}
