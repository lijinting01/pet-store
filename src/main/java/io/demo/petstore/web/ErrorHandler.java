package io.demo.petstore.web;

import org.springframework.hateoas.VndErrors;

class ErrorHandler {

  <T extends Exception> VndErrors onError(T e) {
    String logRef = e.getClass().getSimpleName();
    return new VndErrors(logRef, "Error");
  }
}
