package io.demo.petstore.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author lijinting01
 */
@Controller
@RequestMapping("/")
public class HomeController {

  @GetMapping
  public String index(Model model) {
    model.addAttribute("title", "What can i help u?!");
    return "index";
  }
}
