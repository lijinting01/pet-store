package io.demo.petstore.web;

import io.demo.petstore.account.AccountException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ClientErrorHandler extends ErrorHandler {

  @ExceptionHandler(AccountException.class)
  public VndErrors onAccountException(AccountException e) {
    print(e);
    return onError(e);
  }

  private void print(Exception e) {
    log.error("Exception class: {}, message: {}", e.getClass(), e.getMessage());
  }
}
